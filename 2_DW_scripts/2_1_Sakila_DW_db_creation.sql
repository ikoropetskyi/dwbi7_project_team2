USE [master]
GO

IF EXISTS(select * from sys.databases where name='sakilaDW')
DROP DATABASE sakilaDW;
GO


CREATE DATABASE [sakilaDW]
 ON  PRIMARY 
( NAME = N'sakilaDW', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.IKOR42\MSSQL\DATA\sakilaDW.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'sakilaDW_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.IKOR42\MSSQL\DATA\sakilaDW_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO