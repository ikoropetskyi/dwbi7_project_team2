USE sakilaDW;
GO

--DO Employee table--

CREATE TABLE dbo.Dim_Employee
  (
	employee_key  smallint not null identity,
    employee_BK   tinyint not null,
	full_name     varchar(91) not null,
	is_active     bit not null default(1),
	batch_id      nvarchar(50) null,
	CONSTRAINT PK_Employee PRIMARY KEY (employee_key)
  );
GO

--KM Customer table--
CREATE TABLE dbo.Dim_Customer
  (
	customer_key smallint identity(1,1) not null,
	customer_BK smallint not null,
	full_name varchar(91) not null,
	country varchar(50) not null,
	city varchar(50) not null,
	is_active bit not null default(1),
	batch_id nvarchar(50) null,
	CONSTRAINT PK_Customer PRIMARY KEY (customer_key)
  )
GO

--OM Film table--
CREATE TABLE dbo.Dim_Film (
    film_key smallint IDENTITY(1,1) PRIMARY KEY,
    film_BK smallint NOT NULL,
    title varchar(255) NOT NULL,
    language_name char(20) NOT NULL,
    original_language_name char(20) NULL,
	category_name varchar(25) NOT NULL,
	batch_id nvarchar(50) NULL
);
GO

--OM Actor table--
CREATE TABLE dbo.Dim_Actor(
    actor_key smallint IDENTITY(1,1) PRIMARY KEY,
    actor_BK smallint NOT NULL,
    full_name varchar(91) NOT NULL,
    is_active bit NOT NULL default(1),
	batch_id nvarchar(50) NULL
);

--OM Bridge table--
CREATE TABLE dbo.Dim_Br_Film_Actor (
	film_key smallint NOT NULL,
    actor_key smallint NOT NULL,
	batch_id nvarchar(50) NULL,
	CONSTRAINT PK_Br_Film_Actor PRIMARY KEY (film_key,actor_key));
	

	
--IB Date table--
CREATE TABLE dbo.Dim_Date
  (
	date_key	            int  IDENTITY (1,1) PRIMARY KEY,
    [date]	                date NOT NULL,
	[day]                   int  NOT NULL,
    [week]	                tinyint NOT NULL,
    [week_of_the_month]   	tinyint NOT NULL,
    [month]	                tinyint NOT NULL,
    [month_name]            varchar(15) NOT NULL,
    [year]	                smallint NOT NULL,
    batch_id	            nvarchar(50)  NULL
  );


--IK Fact table--
CREATE TABLE dbo.Fact_Rental_Payment
  (
	rental_id int not null,
    rental_date_key int not null,
	return_date_key int null,
	payment_date_key int null,
	customer_key smallint not null,
	employee_key smallint not null,
	film_key smallint not null,
	amount decimal(5,2) null,
	rental_days smallint null,
	batch_id nvarchar(50) null,
	CONSTRAINT PK_Fact PRIMARY KEY (rental_id)
  );
GO

CREATE TABLE DW_load_metadata
  (
    id int not null identity primary key,
	start_time datetime not null,
	end_time datetime null,
	batch_id nvarchar(50) not null,
	[user] nvarchar(50) not null,
	load_type varchar(15) not null
  );
go



ALTER TABLE dbo.Fact_Rental_Payment ADD  CONSTRAINT FK_Fact_employee FOREIGN KEY(employee_key)
REFERENCES dbo.Dim_Employee (employee_key);
GO

ALTER TABLE dbo.Fact_Rental_Payment ADD  CONSTRAINT FK_Fact_customer FOREIGN KEY(customer_key)
REFERENCES dbo.Dim_Customer (customer_key);
GO

ALTER TABLE dbo.Fact_Rental_Payment ADD  CONSTRAINT FK_Fact_film FOREIGN KEY(film_key)
REFERENCES dbo.Dim_Film (film_key);
GO


ALTER TABLE dbo.Fact_Rental_Payment ADD  CONSTRAINT FK_Fact_rental_date FOREIGN KEY(rental_date_key)
REFERENCES dbo.Dim_Date (date_key);
GO


ALTER TABLE dbo.Fact_Rental_Payment ADD  CONSTRAINT FK_Fact_return_date FOREIGN KEY(return_date_key)
REFERENCES dbo.Dim_Date (date_key);
GO


ALTER TABLE dbo.Fact_Rental_Payment ADD  CONSTRAINT FK_Fact_payment_date FOREIGN KEY(payment_date_key)
REFERENCES dbo.Dim_Date (date_key);
GO


ALTER TABLE dbo.Dim_Br_Film_Actor ADD CONSTRAINT FK_Bridge_Film FOREIGN KEY(film_key)
REFERENCES dbo.Dim_Film (film_key);
GO

ALTER TABLE dbo.Dim_Br_Film_Actor ADD CONSTRAINT FK_Bridge_Actor FOREIGN KEY(actor_key)
REFERENCES dbo.Dim_Actor (actor_key);
GO
