CREATE TABLE dbo.Dim_Actor(
    actor_key smallint IDENTITY(1,1) PRIMARY KEY,
    actor_BK smallint NOT NULL,
    full_name varchar(90) NOT NULL,
    is_active bit NOT NULL,
	batch_id nvarchar(50)
);