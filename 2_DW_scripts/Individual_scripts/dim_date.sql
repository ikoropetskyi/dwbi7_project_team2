
CREATE TABLE Dim_Date
  (
	  date_key	            Int  IDENTITY (1,1) PRIMARY KEY,
    date	                Date NOT NULL,
    week	                Tinyint NOT NULL,
    week_of_the_month   	Tinyint NOT NULL,
    month	                Tinyint NOT NULL,
    year	                Smallint NOT NULL,
    batch_id	            nvachar(50)
  );
  