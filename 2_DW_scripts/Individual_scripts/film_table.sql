CREATE TABLE dbo.Dim_Film (
    film_key smallint IDENTITY(1,1) PRIMARY KEY,
    film_BK smallint NOT NULL,
    title varchar(255) NOT NULL,
    language_name char(20) NOT NULL,
    original_language_name char(20),
	category_name varchar(25) NOT NULL,
	batch_id nvarchar(50)
);