USE sakilaDW;

CREATE TABLE Employee
  (
	employee_key  tinyint not null identity,
    employee_BK   tinyint not null,
	full_name     varchar(90) not null,
	is_active     bit not null,
	batch_id      nvarchar(50) not null,
	CONSTRAINT PK_Employee PRIMARY KEY (employee_key)
  );
