USE sakilaDW;

CREATE TABLE Dim_Customer
  (
	customer_key smallint identity(1,1) not null,
	customer_BK smallint not null,
	full_name varchar(90) not null,
	country varchar(50) not null,
	city varchar(50) not null,
	is_active bit not null,
	batch_id nvarchar(50) not null,
	CONSTRAINT PK_Customer PRIMARY KEY (customer_key)
  )