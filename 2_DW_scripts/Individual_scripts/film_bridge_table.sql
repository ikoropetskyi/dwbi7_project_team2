CREATE TABLE dbo.Dim_Br_Film_Actor (
	film_key smallint NOT NULL PRIMARY KEY,
    actor_key smallint NOT NULL PRIMARY KEY,
	batch_id nvarchar(50) NOT NULL,
	FOREIGN KEY (film_key) REFERENCES dbo.Dim_Film(film_key),
	FOREIGN KEY (actor_key) REFERENCES dbo.Dim_Actor(actor_key)
  );