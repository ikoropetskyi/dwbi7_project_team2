USE sakilaDW;

CREATE TABLE Fact_Rental_Payment
  (
	rental_id int not null,
    rental_date_key int not null,
	return_date_key int null,
	payment_date_key int not null,
	customer_key smallint not null,
	employee_key tinyint not null,
	film_key smallint not null,
	amount decimal(5,2) not null,
	batch_id nvarchar(50) not null,
	CONSTRAINT PK_Fact PRIMARY KEY (rental_id)
  );