USE sakila;
GO

DELETE FROM payment
WHERE payment_id IN (2,3,4);

DELETE FROM rental
WHERE rental_id IN (573,1185);

INSERT INTO [dbo].[rental]
           ([rental_id]
           ,[rental_date]
           ,[inventory_id]
           ,[customer_id]
           ,[return_date]
           ,[staff_id]
           ,[last_update])
 VALUES (16050, CAST(0x0000965F00C8CEA4 AS DateTime), 3299, 245, CAST(0x0000966800B3A894 AS DateTime), 2, GETDATE()),
 (16051, CAST(0x0000965F00C8CEA4 AS DateTime), 3299, 245, CAST(0x0000966800B3A894 AS DateTime), 2, GETDATE());       
GO


INSERT INTO [dbo].[payment]
           ([payment_id]
           ,[customer_id]
           ,[staff_id]
           ,[rental_id]
           ,[amount]
           ,[payment_date]
           ,[last_update])
     VALUES
(16050, 418, 2, 16050, CAST(100.00 AS Decimal(5, 2)), CAST(0x000096B9013EFA98 AS DateTime), GETDATE()),
(16051, 418, 2, 16051, CAST(100.00 AS Decimal(5, 2)), CAST(0x000096B9013EFA98 AS DateTime), GETDATE()),
(16052, 418, 2, NULL, CAST(100.00 AS Decimal(5, 2)), CAST(0x000096B9013EFA98 AS DateTime), GETDATE());
GO

UPDATE payment
SET amount=100, last_update=GETDATE()
WHERE payment_id>20 AND payment_id<=30;
GO

UPDATE country
set country=country+'TEST'
where country_id>=1 and country_id<=20;
go