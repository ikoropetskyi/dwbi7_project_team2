USE [master]
GO

IF EXISTS(select * from sys.databases where name='sakilaStage')
DROP DATABASE sakilaStage;
GO

/****** Object:  Database [sakila]    Script Date: 1/15/2014 11:49:34 AM ******/
CREATE DATABASE [sakilaStage]
 ON  PRIMARY 
( NAME = N'sakilaStage', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.IKOR42\MSSQL\DATA\sakilaStage.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'sakilaStage_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.IKOR42\MSSQL\DATA\sakilaStage_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO