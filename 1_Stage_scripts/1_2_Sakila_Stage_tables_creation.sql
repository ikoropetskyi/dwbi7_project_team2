USE [sakilaStage]
GO

CREATE TABLE [dbo].[actor](
	[actor_id] [smallint] NOT NULL,
	[first_name] [varchar](45) NOT NULL,
	[last_name] [varchar](45) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[actor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[address](
	[address_id] [smallint] NOT NULL,
	[city_id] [smallint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[address_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[category](
	[category_id] [tinyint] NOT NULL,
	[name] [varchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[city](
	[city_id] [smallint] NOT NULL,
	[city] [varchar](50) NOT NULL,
	[country_id] [smallint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[city_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[country](
	[country_id] [smallint] NOT NULL,
	[country] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[country_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[customer](
	[customer_id] [smallint] NOT NULL,
	[first_name] [varchar](45) NOT NULL,
	[last_name] [varchar](45) NOT NULL,
	[address_id] [smallint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[customer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[film](
	[film_id] [smallint] NOT NULL,
	[title] [varchar](255) NOT NULL,
	[language_id] [tinyint] NOT NULL,
	[original_language_id] [tinyint] NULL,
PRIMARY KEY CLUSTERED 
(
	[film_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[film_actor](
	[actor_id] [smallint] NOT NULL,
	[film_id] [smallint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[actor_id] ASC,
	[film_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[film_category](
	[film_id] [smallint] NOT NULL,
	[category_id] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[film_id] ASC,
	[category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[inventory](
	[inventory_id] [int] NOT NULL,
	[film_id] [smallint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[language](
	[language_id] [tinyint] NOT NULL,
	[name] [char](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[language_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[payment](
	[payment_id] [smallint] NOT NULL,
	[customer_id] [smallint] NOT NULL,
	[staff_id] [tinyint] NOT NULL,
	[rental_id] [int] NULL,
	[amount] [decimal](5, 2) NOT NULL,
	[payment_date] [datetime] NOT NULL,
	[payment_date_new] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[payment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[rental](
	[rental_id] [int] NOT NULL,
	[rental_date] [datetime] NOT NULL,
	[inventory_id] [int] NOT NULL,
	[customer_id] [smallint] NOT NULL,
	[return_date] [datetime] NULL,
	[staff_id] [tinyint] NOT NULL,
	[rental_date_new] [date] NOT NULL,
	[return_date_new] [date] NULL
PRIMARY KEY CLUSTERED 
(
	[rental_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[staff](
	[staff_id] [tinyint] NOT NULL,
	[first_name] [varchar](45) NOT NULL,
	[last_name] [varchar](45) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[staff_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--New tables for incremental--

IF EXISTS (SELECT * FROM sys.tables WHERE [name]='Film_Actor_Bridge_Source')
  DROP TABLE Film_Actor_Bridge_Source;
GO

IF EXISTS (SELECT * FROM sys.tables WHERE [name]='extraction_metadata')
  DROP TABLE extraction_metadata;
GO

CREATE TABLE Film_Actor_Bridge_Source
( 
  s_film_key smallint not null,
  s_actor_key smallint not null,
  CONSTRAINT PK_Film_Actor_Bridge_Source PRIMARY KEY (s_film_key,s_actor_key)
);
GO

CREATE TABLE extraction_metadata
  (
	CET DATETIME NULL,
	LSET DATETIME NULL
  );

GO

INSERT INTO extraction_metadata (CET, LSET)
VALUES ('20060101','20060101');
GO


CREATE TABLE [dbo].[stage_rental_deleted](
	[rental_id] [int] NOT NULL,
	[deleted_date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[rental_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[stage_payment_deleted](
	[payment_id] [smallint] NOT NULL,
	[rental_id] [int] NULL,
	[deleted_date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[payment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[Fact_Source](
	[rental_id] [int] NOT NULL,
	[rental_date_key] [int] NOT NULL,
	[return_date_key] [int] NULL,
	[payment_date_key] [int] NULL,
	[customer_key] [smallint] NOT NULL,
	[employee_key] [smallint] NOT NULL,
	[film_key] [smallint] NOT NULL,
	[amount] [decimal](5, 2) NULL,
	[rental_days] [smallint] NULL
CONSTRAINT [PK_Fact] PRIMARY KEY CLUSTERED 
(
	[rental_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE dbo.stage_load_logs
  (
    id int not null identity primary key,
	start_time datetime not null,
	end_time datetime null,
	[user] nvarchar(50) not null,
	load_type varchar(15) not null
  );
go